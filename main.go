//+build !test

package main

import "fmt"

func main() {
	post, err := decode("post.json")
	if err != nil {
		fmt.Printf("error decoding: %s\n", err)
		return
	}

	fmt.Printf("data: %v\n", post)
}
